# alpine-jellyfin-builder-ffmpeg
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-jellyfin-builder-ffmpeg)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-jellyfin-builder-ffmpeg)



----------------------------------------
### x64
![Docker Image Version](https://img.shields.io/docker/v/forumi0721/alpine-jellyfin-builder-ffmpeg/latest)
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-jellyfin-builder-ffmpeg/latest)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [FFmpeg](https://ffmpeg.org/) for jellyfin (Build Image)
    - FFmpeg is a free software project that produces libraries and programs for handling multimedia data.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

